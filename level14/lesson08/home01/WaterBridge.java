package com.javarush.test.level14.lesson08.home01;

/**
 * Created by Михаил on 22.12.2015.
 */
public class WaterBridge implements Bridge
{
    @Override
    public int getCarsCount()
    {
        return 12;
    }
}
