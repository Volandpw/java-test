package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Михаил on 22.12.2015.
 */
public class MoldovanHen extends Hen
{ public int getCountOfEggsPerMonth()
{
    return 200;
}
    String getDescription()
    {
        return super.getDescription()+" Моя страна - "+ Country.MOLDOVA +". Я несу " +getCountOfEggsPerMonth()+ " яиц в месяц.";
    }
}
