package com.javarush.test.level04.lesson16.home02;

import java.io.*;

/* Среднее такое среднее
Ввести с клавиатуры три числа, вывести на экран среднее из них. Т.е. не самое большое и не самое маленькое.
*/

public class Solution
{
    public static void main(String[] args)   throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String a = reader.readLine();
        int num1 = Integer.parseInt(a);
        String b = reader.readLine();
        int num2 = Integer.parseInt(b);
        String c = reader.readLine();
        int num3 = Integer.parseInt(c);

        if ((num1>=num2)&&(num2>=num3))
            System.out.print(num2);
        else if((num1>=num3)&&(num2<=num3))
            System.out.print(num3);
        else if((num3<=num1)&&(num2>=num1))
            System.out.print(num1);
        //напишите тут ваш код
    }
}
