package com.javarush.test.level08.lesson08.task04;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution
{
    public static HashMap<String, Date> createMap()
    {
        HashMap<String, Date> map = new HashMap<String, Date>();
        map.put("Stallone", new Date("JUNE 1 1980"));
        map.put("Serz2", new Date ("MAY 1 1982"));
        map.put("Serz23", new Date ("MAY 2 1982"));
        map.put("Serz32", new Date ("JUNE 1 1995"));
        map.put("Serz323", new Date ("JUNE 3 1995"));
        map.put("Serz1232", new Date ("JUNE 5 1995"));
        map.put("Serz7832", new Date ("JUNE 6 1995"));
        map.put("Serz8732", new Date ("JUNE 7 1995"));
        map.put("Serz3209", new Date ("JUNE 8 1995"));
        map.put("Serz3298", new Date ("JUNE 15 1995"));

        //напишите тут ваш код
        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map)
    {
        Iterator<Map.Entry<String , Date>> iterator = map.entrySet().iterator();

        while(iterator.hasNext())
        {
            Map.Entry<String , Date> pair = iterator.next();

                if(pair.getValue().getMonth() == 5|| pair.getValue().getMonth() == 6|| pair.getValue().getMonth() == 7)
                iterator.remove();

        }
        //напишите тут ваш код

    }
}
