package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String , String> map = new HashMap<String, String>();

        map.put("Mikebnm","addmnb");
        map.put("Mikebbb","add123");
        map.put("Mike12w3","addmn");
        map.put("Mikebj","addw");
        map.put("Mike","add123add");
        map.put("Mike123123","addhj");
        map.put("Mikezxcv","add");
        map.put("Mikezxc","addzxc");
        map.put("Mikezxsa","addzxcv");
        map.put("Mi123ke","ad123d");

        //напишите тут ваш код
        return map;

    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        int count = 0;

        Iterator<Map.Entry<String , String>> iterator = map.entrySet().iterator();

        while (iterator.hasNext())
        {
            Map.Entry<String, String> pair = iterator.next();

            String key = pair.getValue();
            if (key.equals(name))
                count++;

        }

        //напишите тут ваш код
        return count;

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String familiya)
    {
        int count = 0;

        Iterator<Map.Entry<String , String>> iterator = map.entrySet().iterator();

        while (iterator.hasNext())
        {
            Map.Entry<String, String> pair = iterator.next();

            String value = pair.getKey();
            if (value.equals(familiya))
                count++;
        }

        //напишите тут ваш код
        return count;



    }
}
