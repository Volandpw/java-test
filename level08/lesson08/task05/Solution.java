package com.javarush.test.level08.lesson08.task05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> map = new HashMap<String, String>();

        map.put("qwe", "123");
        map.put("qwer", "11223");
        map.put("qwert", "1231");
        map.put("qwerty", "123321");
        map.put("q", "1");
        map.put("qw", "12");
        map.put("qwea", "123a");
        map.put("qweas", "123as");
        map.put("qweasd", "123asd");
        map.put("qweasdf", "123asdf");



        return map;
        //напишите тут ваш код
    }



    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {


        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();

        HashMap<String, String> copy1 = new HashMap<String, String>(map);

        for (Map.Entry<String, String> pair : copy1.entrySet())
        {
            int n = 0;
            for (Map.Entry<String, String> p : copy1.entrySet())
            {
                if (pair.getValue().equals(p.getValue())) n++;
            }
            if (n >= 2) removeItemFromMapByValue(map, pair.getValue());
        }
        //напишите тут ваш код

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}
