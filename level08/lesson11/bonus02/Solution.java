package com.javarush.test.level08.lesson11.bonus02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/* Нужно добавить в программу новую функциональность
Задача: Программа определяет, какая семья (фамилию) живёт в доме с указанным номером.
Новая задача: Программа должна работать не с номерами домов, а с городами:
Пример ввода:
Москва
Ивановы
Киев
Петровы
Лондон
Абрамовичи

Лондон

Пример вывода:
Абрамовичи
*/

public class Solution
{
    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Map <String , String> map = new HashMap<String , String>();

        //list of addresses

        while (true)
        {
            String family = reader.readLine();
            if (family.isEmpty()) break;
            String sity = reader.readLine();
            if (sity.isEmpty()) break;
            map.put(family, sity);
        }

        Iterator<Map.Entry<String , String>> iterator = map.entrySet().iterator();

        //read home number
        String tempSity = reader.readLine();
        while(iterator.hasNext())

        {
            Map.Entry<String, String> pair = iterator.next();

            String value = pair.getValue();
            String key = pair.getKey();


            if(tempSity.equals(key))
            System.out.println(value);
        }

    }
}
