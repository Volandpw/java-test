package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {
        Human boy1 = new Human("boy", true,5, null);
        Human boy2 = new Human("132", true,6, null);
        Human boy3 = new Human("boy3", true,5, null);

        Human mother = new Human("Mother", false, 32, boy1);

        Human father = new Human("father", true, 33,boy3);

        Human grandM = new Human("GMoth", false, 60, mother);

        Human grandM2 = new Human("GMoth2", false, 69, father);

        Human grandF = new Human("GMoth12", true, 90, mother);
        Human grandF2 = new Human("GMoth132", true, 93, father);

        ArrayList<Human> family = new ArrayList<Human>();

        family.add(boy1);
        family.add(boy2);
        family.add(boy3);
        family.add(mother);
        family.add(father);
        family.add(grandM);
        family.add(grandM2);
        family.add(grandF);
        family.add(grandF2);

        for (int i = 0; i < family.size(); i++)
            System.out.println(family.get(i));




        //напишите тут ваш код
    }

    public static class Human
    {
        private String name;
                Boolean sex;
                int age;
        /**
         * list - children
         */
                ArrayList<Human> children = new ArrayList<Human>();

        public Human(String name, boolean sex, int age, Human h)
        {
            this.age = age;
            this.name = name;
            this.sex = sex;

            if (h!=null)
                children.add(h);
            this.children= new ArrayList<Human>();

        }



        //напишите тут ваш код


        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
