package com.javarush.test.level05.lesson12.bonus03;

import java.io.*;

/* Задача по алгоритмам
Написать программу, которая:
1. вводит с консоли число N > 0
2. потом вводит N чисел с консоли
3. выводит на экран максимальное из введенных N чисел.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        int maximum = -1000000000 ;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();
        int n = Integer.parseInt(s);
        int[]mas = new int[n];
        for(int i = 0; i < mas.length; i++)
        {
            String s1 = reader.readLine();
            mas[i] = Integer.parseInt(s1);
        }

        for(int i = 0; i < mas.length; i++)
        {
            if (mas[i] >= maximum)
                maximum = mas[i];
        }


        //напишите тут ваш код



        System.out.println(maximum);
    }
}
