package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
 Создай объекты и заполни их так, чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/


import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {//Написать тут ваш код
        ArrayList<Human> fam = new ArrayList<Human>();
        fam.add(0,new Human("Сергей",true,58));
        fam.add(1,new Human("Николай",true,47));
        fam.add(2,new Human("Алла",false,54));
        fam.add(3,new Human("Ольга",false,44));
        fam.add(4,new Human(fam.get(0),fam.get(2),"Дмитрий",true,28));
        fam.add(5,new Human(fam.get(1),fam.get(3),"Ирина",false,24));
        fam.add(6,new Human(fam.get(4),fam.get(5),fam.get(0),fam.get(2),fam.get(1),fam.get(3),"Доминика",false,3));
        fam.add(7,new Human(fam.get(4),fam.get(5),fam.get(0),fam.get(2),fam.get(1),fam.get(3),"Виктория",false,1));
        fam.add(6,new Human(fam.get(4),fam.get(5),fam.get(0),fam.get(2),fam.get(1),fam.get(3),"Сергей",true,1));

        for (int i = 0; i <fam.size() ; i++)
        {
            fam.get(i).toString();
        }
    }

    public static class Human
    {
        private String name;
        private boolean sex;
        private int age;
        private Human father;
        private Human mother;
        private  Human gfather_1;
        private Human gmother_1;
        private  Human gfather_2;
        private Human gmother_2;

        Human(String name,boolean sex,int age)
        {
            this.name = name;
            this.sex =sex;
            this.age = age;
        }
        Human(Human father, Human mother, String name, boolean sex, int age)
        {
            this.name = name;
            this.sex =sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        Human(Human father, Human mother,Human gfather_1,Human gmother_1,Human gfather_2,Human gmother_2, String name, boolean sex, int age)
        {
            this.name = name;
            this.sex =sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
            this.gfather_1 = gfather_1;
            this.gfather_2 = gfather_2;
            this.gmother_1 =gmother_1;
            this.gmother_2 = gmother_2;
        }
        public String toString ()
        {


            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;


            if (this.gfather_1 != null)
                text += ", дедушка: " + this.gfather_1.name;


            if (this.gmother_1 != null)
                text += ", бабушка: " + this.gmother_1.name;

            if (this.gfather_2 != null)
                text += ", дедушка: " + this.gfather_2.name;


            if (this.gmother_2 != null)
                text += ", бабушка: " + this.gmother_2.name;
            System.out.println(text);

            return text;
        }
    }
}