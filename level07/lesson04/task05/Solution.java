package com.javarush.test.level07.lesson04.task05;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/* Один большой массив и два маленьких
1. Создать массив на 20 чисел.
2. Ввести в него значения с клавиатуры.
3. Создать два массива на 10 чисел каждый.
4. Скопировать большой массив в два маленьких: половину чисел в первый маленький, вторую половину во второй маленький.
5. Вывести второй маленький массив на экран, каждое значение выводить с новой строки.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int[] mas = new  int[20];
        int[] masYoungerPart = new int[10];
        int[] masOlderPart = new int[10];
        int[] mashelp = new int[10];
        for(int i = 0; i < mas.length; i++)
        {
            String s = reader.readLine();
            mas[i] = Integer.parseInt(s);
        }
        for(int i = 0; i < (mas.length)/2; i++)
        {
            masYoungerPart[i] = mas[i];
        }
        for(int i =mas.length/2, a = 0; i < mas.length;i++, a++ )
        {
                masOlderPart[a] = mas[i];

        }
        for(int i = 0; i < masOlderPart.length; i++ )
            System.out.println(masOlderPart[i]);
        //напишите тут ваш код

    }
}
